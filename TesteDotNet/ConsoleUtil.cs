﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteDotNet
{
    public abstract class ConsoleUtil
    {
        public static void ApresentarErro(string mensagem)
        {
            var corAnterior = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(mensagem);
            Console.ForegroundColor = corAnterior;
        }
    }
}
