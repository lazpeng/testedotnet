﻿using System;

namespace TesteDotNet.Exceptions
{
    public class BaseOperacaoException : Exception
    {
        public BaseOperacaoException() : base() { }

        public BaseOperacaoException(string Message) : base(Message) { }
    }
}
