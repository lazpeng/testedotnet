﻿using System;

namespace TesteDotNet.Exceptions
{
    /// <summary>
    /// Representa uma exceção causada por número incorreto de argumentos dado a uma operação
    /// </summary>
    public class OperacaoArgumentoException : BaseOperacaoException
    {
        public OperacaoArgumentoException() : base() { }

        public OperacaoArgumentoException(string Message) : base(Message) { }
    }
}
