﻿using System;
using System.Threading;

namespace TesteDotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bem vindo!");

            bool sair = false;

            while(!sair)
            {
                new Calculadora().Iniciar();

                Console.WriteLine("Pressione qualquer tecla para continuar ou ESC para sair.");
                sair = Console.ReadKey(true).Key == ConsoleKey.Escape;
            }

            Console.WriteLine("Obrigado por utilizar nossa calculadora!");
            Thread.Sleep(TimeSpan.FromSeconds(2));
        }
    }
}
