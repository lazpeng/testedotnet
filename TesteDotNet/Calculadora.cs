﻿using System;
using System.Collections.Generic;
using System.IO;
using TesteDotNet.Enums;
using TesteDotNet.Exceptions;

namespace TesteDotNet
{
    public class Calculadora
    {
        private readonly Dictionary<string, Func<long, long, long>> DictOperacoesSimples = new Dictionary<string, Func<long, long, long>>()
        {
            ["SOMA"] = Operacoes.Soma,
            ["DIVISÃO"] = Operacoes.Divisao,
            ["MULTIPLICAÇÃO"] = Operacoes.Multiplicacao,
            ["SUBTRAÇÃO"] = Operacoes.Subtracao
        };

        private readonly Dictionary<string, Func<long[], long>> DictOperacoesComplexas = new Dictionary<string, Func<long[], long>>()
        {
            ["SOMA"] = Operacoes.Soma,
            ["MÉDIA"] = Operacoes.Media,
            ["SOMA PARES"] = Operacoes.SomaPares
        };

        /// <summary>
        /// Apresenta o menu inicial ao usuário e lê a resposta digitada em console
        /// </summary>
        /// <returns>Um valor válido de TipoOperacao caso uma resposta válida tenha sido dada, ou null caso contrário</returns>
        private TipoOperacao? ApresentarMenuPrincipal()
        {
            Console.Write("Selecione uma opção:\n\t1 - Realizar operação\n\t2 - Ler operações de um arquivo\n>");

            if (int.TryParse(Console.ReadLine(), out int selecao))
            {
                return selecao switch
                {
                    1 => TipoOperacao.RealizarOperacao,
                    2 => TipoOperacao.LerArquivo,
                    _ => null
                };
            }

            return null;
        }

        /// <summary>
        /// Mostra os resultados obtidos na operação que acabou de ser realizada e o requisitante, caso haja um
        /// </summary>
        /// <param name="detalhe">Detalhe utilizado para executar a operação</param>
        /// <param name="resultado">O resultado obtido com a execução da operação e argumentos passados no detalhe</param>
        private void ApresentarResultadoOperacao(DetalheOperacao detalhe, long resultado)
        {
            if(!string.IsNullOrEmpty(detalhe.Requisitante))
            {
                Console.WriteLine($"[{detalhe.Requisitante}]");
            }
            Console.WriteLine($"Resultado da operação de {detalhe.Operacao} : {resultado}");
            Console.WriteLine("Valores utilizados:");

            for (int i = 0; i < detalhe.Argumentos.Length; ++i)
            {
                Console.WriteLine($"{i + 1}º : {detalhe.Argumentos[i]}");
            }
            // Espaçamento entre os resultados
            Console.WriteLine();
        }

        /// <summary>
        /// Executa uma operação simples que toma apenas dois argumentos
        /// </summary>
        /// <param name="operacao">A função da operação obtida no dicionário de operações simples</param>
        /// <param name="detalhe">Detalhe que contém as informações necessárias para executar a operação</param>
        /// <returns>O resultado da execução da operação</returns>
        private long ExecutarOperacao(Func<long, long, long> operacao, DetalheOperacao detalhe)
        {
            if (detalhe.Argumentos.Length != 2)
            {
                throw new OperacaoArgumentoException($"A operação seleciona espera dois argumentos, porém {detalhe.Argumentos.Length} foram passados");
            }

            var resultado = operacao(detalhe.Argumentos[0], detalhe.Argumentos[1]);
            ApresentarResultadoOperacao(detalhe, resultado);
            return resultado;
        }


        /// <summary>
        /// Executa uma operação "complexa" que toma um número ilimitado de argumentos
        /// </summary>
        /// <param name="operacao">A função da operação obtida no dicionário de operações complexas</param>
        /// <param name="detalhe">Detalhe que contém as informações necessárias para executar a operação</param>
        /// <returns>O resultado da execução da operação</returns>
        private long ExecutarOperacao(Func<long[], long> operacao, DetalheOperacao detalhe)
        {
            if (detalhe.Argumentos.Length == 0)
            {
                throw new OperacaoArgumentoException("Operação tem um número ilimitado de argumentos, porém nenhum foi passado.");
            }

            var resultado = operacao(detalhe.Argumentos);
            ApresentarResultadoOperacao(detalhe, resultado);
            return resultado;
        }

        /// <summary>
        /// Segue o que foi preenchido no detalhe para executar a operação informada com os argumentos informados, retornando o resultado obtido
        /// </summary>
        /// <param name="detalhe">Detalhe que contém as informações necessárias para executar a operação</param>
        /// <returns>Resultado da operação</returns>
        private long ProcessarDetalheOperacao(DetalheOperacao detalhe)
        {
            long resultado = 0;
            if(DictOperacoesSimples.TryGetValue(detalhe.Operacao, out Func<long, long, long> simples))
            {
                // Quando a operação existe no dicionário de operações simples porém ocorre um erro devido ao número de argumentos, verificar se existe no
                // dicionário de operações complexas, pois pode existir uma variante que recebe argumentos indefinidos.
                try
                {
                    resultado = ExecutarOperacao(simples, detalhe);
                }
                catch (OperacaoArgumentoException)
                {
                    if(DictOperacoesComplexas.TryGetValue(detalhe.Operacao, out Func<long[], long> complexa))
                    {
                        resultado = ExecutarOperacao(complexa, detalhe);
                    }
                    else throw new Exception($"Operação {detalhe.Operacao} não encontrada");
                }
            }
            else if (DictOperacoesComplexas.TryGetValue(detalhe.Operacao, out Func<long[], long> complexa))
            {
                resultado = ExecutarOperacao(complexa, detalhe);
            }
            else throw new Exception($"Operação {detalhe.Operacao} não encontrada");

            return resultado;
        }

        /// <summary>
        /// Apresenta uma mensagem simples e toma o input do usuário para a execução de uma operação com os parâmetros digitados em console,
        /// executando a operação e apresentando o resultado
        /// </summary>
        private void ApresentarMenuOperacao()
        {
            Console.Write("Entre os argumentos da operação no seguinte formato: Nome da operação;<argumento 1>;<argumento 2>;...\n>");
            try
            {
                ProcessarDetalheOperacao(DetalheOperacao.Parse(Console.ReadLine()));
            } catch (Exception e)
            {
                ConsoleUtil.ApresentarErro(e.Message);
            }
        }

        /// <summary>
        /// Toma do usuário o arquivo a ser lido e realiza a execução dos comandos encontrados no arquivo
        /// </summary>
        private void ApresentarMenuArquivo()
        {
            try
            {
                Console.Write("Nome do arquivo a ser aberto:\n>");
                var nome = Console.ReadLine();

                var dictPessoas = new Dictionary<string, long>();

                if (!File.Exists(nome))
                {
                    throw new Exception($"Arquivo \"{nome}\" não encontrado");
                }

                foreach (var linha in File.ReadAllLines(nome))
                {
                    if (string.IsNullOrEmpty(linha.Trim()))
                        continue;

                    var detalhe = DetalheOperacao.Parse(linha);
                    if(string.IsNullOrEmpty(detalhe.Requisitante))
                    {
                        throw new Exception($"Erro: Na linha \"{linha}\" não foi informado o Usuário/Requisitante da operação.");
                    }
                    dictPessoas[detalhe.Requisitante] = ProcessarDetalheOperacao(detalhe);
                }

                Console.WriteLine("Resultados por requisitante:");
                foreach(var requisitante in dictPessoas.Keys)
                {
                    Console.WriteLine($"{requisitante} : {dictPessoas[requisitante]}");
                }
            }
            catch (Exception e)
            {
                ConsoleUtil.ApresentarErro($"Ocorreu um erro durante a leitura do arquivo: {e.Message}");
            }
        }

        /// <summary>
        /// Ponto de entrada da calculadora, que decide no ponto de partida de acordo com a resposta do usuário e passa o controle
        /// para o fluxo escolhido pelo usuário, ou apresenta as opções novamente caso uma opção inválida tenha sido digitada
        /// </summary>
        public void Iniciar()
        {
            TipoOperacao operacaoPrincipal;

            while (true)
            {
                var selecao = ApresentarMenuPrincipal();
                if (selecao != null)
                {
                    operacaoPrincipal = selecao.Value;
                    break;
                }
                ConsoleUtil.ApresentarErro("Opção não compreendida. Tente novamente.");
            }

            switch (operacaoPrincipal)
            {
                case TipoOperacao.LerArquivo:
                    ApresentarMenuArquivo();
                    break;
                case TipoOperacao.RealizarOperacao:
                    ApresentarMenuOperacao();
                    break;
            }
        }
    }
}
