﻿using System.Linq;

namespace TesteDotNet
{
    public abstract class Operacoes
    {
        public static long Soma(long A, long B)
        {
            return A + B;
        }

        public static long Soma(long[] Argumentos)
        {
            return Argumentos.Aggregate((a, b) => a + b);
        }

        public static long SomaPares(long[] Argumentos)
        {
            return Soma(Argumentos.Where(n => n % 2 == 0).ToArray());
        }

        public static long Media(long[] Numeros)
        {
            return Soma(Numeros) / Numeros.Length;
        }

        public static long Subtracao(long A, long B)
        {
            return A - B;
        }

        public static long Divisao(long A, long B)
        {
            return A / B;
        }

        public static long Multiplicacao(long A, long B)
        {
            return A * B;
        }
    }
}
