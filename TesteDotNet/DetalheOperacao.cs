﻿using System;
using System.Collections.Generic;

namespace TesteDotNet
{
    public class DetalheOperacao
    {
        public string Requisitante { get; set; }
        public string Operacao { get; set; }
        public long[] Argumentos { get; set; }

        /// <summary>
        /// Faz o _parsing_ de uma especificação de operação e preenche um objeto de detalhe com os devidos argumentos
        /// passados por ponto e vírgula.
        /// </summary>
        /// <param name="fonte">Especificação da operação a ser realizada, composta de parâmetros separador por ponto e vírgula</param>
        /// <returns>Uma instância da classe DetalheOperacao com </returns>
        public static DetalheOperacao Parse(string fonte)
        {
            var valores = fonte.Split(';');

            if(valores.Length < 2)
            {
                throw new Exception("Número de argumentos separados por ; é inválido. Não há argumentos suficientes.\nDeve seguir o formato Operacao;<argumento 1>;<argumento 2>;...");
            }

            // Operação é obrigatório, mas o nome do requisitante pode ser omitido.
            // Formatos válidos são:
            // Operacao;<argumento 1>;<argumento 2>
            // ou
            // Nome;Operacao;<argumento 1>;<argumento 2>
            // Com um número ilimitado de argumentos
            // 
            // Caso o segundo elemento seja numérico, é a primeira forma. Caso contrário, a segunda.
            // Ou seja: caso o elemento 1 seja um número, a operação está no elemento 0. Caso contrário,
            // a operação se encontra no elemento 1 (e no 0 está o nome do requisitante/usuário da operação)

            int index = 0;

            string requisitante = "";

            if (!char.IsDigit(valores[1][0]))
            {
                requisitante = valores[0];
                index += 1;
            }

            string operacao = valores[index].ToUpper();
            index += 1;

            var argumentos = new List<long>();

            for (; index < valores.Length; ++index)
            {
                if (long.TryParse(valores[index], out long valor))
                {
                    argumentos.Add(valor);
                }
                else throw new Exception($"Argumento para operação na posição {index} não é válido");
            }

            return new DetalheOperacao
            {
                Operacao = operacao,
                Requisitante = requisitante,
                Argumentos = argumentos.ToArray()
            };
        }
    }
}
